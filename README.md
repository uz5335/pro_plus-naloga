# Pro_Plus:naloga

Implmentacija naloga ProPlus, branje/parsanje/prikazovanje RSS feed, od DARS-a, .

##Mongo remote
//S spodnjimi ukazi se dobi zadnja verzija mongota, za remote povezavo do mLab potrebujete verzijo monga vsaj >= 3.0  
$ mongo --version  
$ sudo apt-get remove mongodb-org mongodb-org-server  
$ sudo apt-get autoremove  
$ sudo rm -rf /usr/bin/mongo*  
$ echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list  
$ sudo apt-get update  
$ sudo apt-get install mongodb-org mongodb-org-server  
$ sudo touch /etc/init.d/mongod  
$ sudo apt-get install mongodb-org-server  
  
$ mongo ds019996.mlab.com:19996/rrs_pro_plus -u uz5335 -p 123  (ukaz za oddaljen dostop do baze)  

Sama shema je vidna v ./db/schemas.js  

##Install/run
$ npm install  
$ npm rur  
$ npm start  

//-->localhost:3000  

##Docker run  

docker pull uz5335/proplus  
docker images  
docker run uz5335/proplus  
  
//-->localhost:3000  
* LOKALEN BUILD, KI SIGURNO DELUJE (postavi se v mapo repozitorija, kjer je Dockerfile)  
docker build -t proplus .  
docker run -p 3000:3000 proplus  
  
//-> localhost:3000

##API klici

* /cron  
vrne status 200 in json sporocilo da je ok. Zapiše trenutna sporočila v bazo...starih se znebim.
* /events  
vrne vse zapise v bazi  
* /events?highwayOnly=true  
vrne samo zapise z oznako A (avtocesta)  
* /events?highwayOnly=false  
isto kot /events  
* /  
dostop do web-app. prikaže vmesnik z grafičnim prikazom trenutnih RSS.

