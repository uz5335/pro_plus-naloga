(function() {

    var rss123 = function() {
        return {
            restrict: 'EA',
            scope: {
                rss123: '=rss123'
            },
            templateUrl: "/directives/rss.html",
        };
    };

    angular
        .module('proplus')
        .directive('rss123', rss123);
})();