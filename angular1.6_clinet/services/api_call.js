(function () {
    /* global angular */

    function api_call($http) {

    

        let events = function (flag) {
            return $http.get('/events?highwayOnly='+flag);
        }

       
        return {
            events:events,
            
        };
    }

    angular
        .module('proplus')
        .service('api_call', api_call);
})();