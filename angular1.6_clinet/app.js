(function () {
    angular.module('proplus', ['ngRoute']); 
    function nastavitev($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'webPage/web.html',
                controller: 'webCtrl',
                controllerAs: 'vm'
            })

            .otherwise({redirectTo: '/'});

        $locationProvider.html5Mode(true);
    }
    angular
        .module('proplus')
        .config(['$routeProvider', '$locationProvider', nastavitev]);
})();