var express = require('express');
var router = express.Router();
var controller = require('./controllers/feedCtr');

router.get('/', controller.getFeed);

module.exports = router;
