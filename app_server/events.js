var express = require('express');
var router = express.Router();
var controller = require('./controllers/eventsCtrl');

router.get('/', controller.getEvents);

module.exports = router;
