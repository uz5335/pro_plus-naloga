var FeedParser = require('feedparser');
var request = require('request'); // for fetching the feed
const mongoose = require("mongoose");
const Rss = mongoose.model("RSS");

module.exports.getFeed = function(req, res){
  var req = request('https://www.promet.si/dc/b2b.dogodki.rss?language=sl_SI&eventtype=incidents')
  var feedparser = new FeedParser([]);
   let result = [];
 req.on('error', function (error) {
   // handle any request errors
 });

 req.on('response', function (res1) {
   var stream = this; // `this` is `req`, which is a stream

   if (res1.statusCode !== 200) {
     this.emit('error', new Error('Bad status code'));
   }
   else {
     stream.pipe(feedparser);
   }
 });


 feedparser.on('error', function (error) {
   // always handle errors
 });

 feedparser.on('readable', function () {
   // This is where the action is!
   var stream = this; 
   var meta = this.meta; 
   while (item = stream.read()) {
     getRouteType(item.title.charAt(0), function(rs){
        if(rs){
          result.push(item);       
        }
     });
    
   }

 });

 feedparser.on('finish', function () {
     console.log('feedparser finish event');
     /*console.log(result[0]);*/
     console.log("\n\n Dolzina: ", result.length)

      Rss.remove({}, function(err) { 
          if(err){
              res.status(400).json(err);
          }else{

             for(let i = 0; i < result.length; i++){
                let rss = new Rss({
                                title: result[i].title, 
                                description: result[i].description,
                                routeType: result[i].title.charAt(0),
                                 guid: Number(result[i].guid),
                                 categories: result[i].categories,
                                 date: result[i].date
                              });
                rss.save(function(error, responseUser){
                    if(error){
                        res.status(500).json(error);
                    }else{
                      //console.log("i: ", i, " d: ", result.length-1);
                      if(result.length - 1 === i){
                         res.status(200).json({ok: "ok"});
                      }
                    }
                });

             }

          }
      });
        

 });
 //console.log("neki");
}

const getRouteType = function(routeType, cb){
  if(routeType == "A" || routeType == "G"){
    cb(true);
  }else{
    cb(false);
  }

}
