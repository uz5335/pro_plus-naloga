//TODO: RIP slovenscina, ES6
var mongoose = require("mongoose");
 //---->lokalna povezava

var dbURI = "mongodb://uz5335:123@ds019996.mlab.com:19996/rrs_pro_plus";

mongoose.connect(dbURI);

mongoose.connection.on("connected", function() {
    console.log("Mongoose je povezan na " + dbURI);
});

mongoose.connection.on("error", function(err) {
    console.log("Mongoose connection error: " + err);
});
mongoose.connection.on("disconnected", function() {
    console.log("Mongoose je zaprl povezavo");
});

var pravilnaUstavitev = function(sporocilo, povratniKlic) {
    mongoose.connection.close(function() {
        console.log("Mongoose je zaprl povezavo preko " + sporocilo);
        povratniKlic();
    });
};

// Pri ponovnem zagonu nodemon
process.once("SIGUSR2", function() {
    pravilnaUstavitev("nodemon ponovni zagon", function() {
        process.kill(process.pid, "SIGUSR2");
    });
});

// Pri izhodu iz aplikacije
process.on("SIGINT", function() {
    pravilnaUstavitev("izhod iz aplikacije", function() {
        process.exit(0);
    });
});

// Pri izhodu iz aplikacije na Heroku
process.on("SIGTERM", function() {
    pravilnaUstavitev("izhod iz aplikacije na Heroku", function() {
        process.exit(0);
    });
});

require("./schemas");
