const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const userShema = new Schema({
        title: {
            type: String,
            required: true
        },
        routeType:{
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        guid:{
            type: Number,
            required: true
        },
        categories: {
            type: []
        },
        date: {
            type: Date,
        }
 
    },
    {
        versionKey: false
    });



mongoose.model("RSS", userShema);

console.log('schemas created');
